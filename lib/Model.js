/*
 * The Model object
 *
 * All models are created using Model.create.
 *
 */

Model = {

    /**
     * Meta-programming wins.
     */

    create: function (collectionName, defaults, prototype) {

        var constructor;
        if (prototype && typeof prototype.construct == "function") {
            constructor = prototype.construct;
        } else {
            // default constructor.
            // extends this with the doc
            constructor = function (doc) {
                _.extend(this, doc);
            };
        }

        function M(doc) {
            _.extend(this, M.defaults);
            _.extend(this, doc);

            // TODO: try to convert most data types.
            // e.g date time etc.

            constructor.call(this, [doc]);
        }

        M.defaults = defaults;

        M.collection = new Meteor.Collection(collectionName,
            {
                transform: function (doc) {
                    return new M(doc);
                }
            }
        );

        // Extend the model with model primitives

        _.extend(M, Model);

        _.extend(M.prototype, {
            save: function () {
                var doc = {};

                // TODO:
                // try to convert to serializable json
                // minimongo already does this to some extent

                for (field in M.defaults) {
                    if (M.defaults.hasOwnProperty(field)) {

                        // here we rely on the constructor
                        // to have filled default values.

                        if (typeof this[field] != "function") {
                            doc[field] = this[field];
                        }
                    }
                }

                for (field in M.defaults) {
                    if (M.defaults.hasOwnProperty(field)) {
                        // run the function, store the output.
                        if (typeof this[field] == "function") {
                            doc[field] = this[field]();
                        }
                    }
                }

                if (this._id === undefined) {
                    this._id = M.collection.insert(doc);
                } else {
                    M.collection.update(
                            {_id: this._id}, {$set: doc}
                    );
                }
            }
        });


        // Now load the prototype
        // Gives a chance for models to override save.

        _.extend(M.prototype, prototype);

        return M;
    },

    /*
     * Suppose A.hasMany(B); Then:
     *
     * a.bs()               gives all Bs associated with instance a
     * a.hasB(b)            checks if a has B b.
     * a.addB(b, edgeData)  adds to the colelction
     *
     * Also:
     *
     * b.as()               gives all As containing b (eg. projects with user u)
     * b.inA(a)           checks if a has B b in it (eg. user in project)
     */

    hasMany: function (model, options) {
        var L = this,
            R = model;

        if (options === undefined) {
            options = {};
        }

        function getOpt(opt, def) {
            return options[opt] || def;
        }

        // Getting the English right.
        var rPlural   = getOpt("plural", R.collection._name),
            rSingular = getOpt("singular", rPlural.replace(/s$/, "")),
            lPlural   = getOpt("lPlural", this.collection._name),
            entities  = "_" + rPlural,
            lSingular = getOpt("lSingular", lPlural.replace(/s$/, ""));

        function getId(obj) {
            if (typeof obj == "object") {
                if (typeof obj._id == "undefined") {
                    throw "The object you are trying to add doesn't have an ID. " +
                              + "Is it saved?";
                } else {
                    return obj._id;
                }
            } else if (typeof obj == "string") {
                return obj;
            }
            throw "Trying to add an invalid object.";
        }

        L.defaults[entities] = [];

        // add a member
        var addR = "add" + Util.ucfirst(rSingular);
        L.prototype[addR] = function (obj) {
            this[entities].push(getId(obj));
        };

        var removeR = "remove" + Util.ucfirst(rSingular);
        L.prototype[removeR] = function (obj) {
            this[entities] = _.without(this[entities], getId(obj));
        };

        var hasR = "has" + Util.ucfirst(rSingular);
        L.prototype[hasR] = function (obj) {
            return getId(obj) in this[entites];
        };

        // get all
        L.prototype[rPlural] = function () {
            return R.collection.find({_id: {$in: this[entities]}}).fetch();
        };

        // get all lefts - this is slower than the above.
        R.prototype[lPlural] = function () {
            var query = {};
            query[entities] = {$in: [this._id]};
            return L.collection.find(query).fetch();
        }
    },

    /**
     * hasManyI: indirectly has many
     *
     * User.hasManyI("tasks.messages")
     *
     */
    hasManyI: function (attr, model, options) {

        if (options === undefined) {
            options = {};
        }
        var path = attr.split("."),
            rPlural   = options["plural"] || _.last(path);

        if (path.length != 2) {
            throw "Don't know how to handle path " +
                  attr + " for indirectly many."
        }

        this.prototype[rPlural] = function () {
            var first = _.map(this[path[0]](),
               function (t) {
                   return t[path[1]]();
               });

            var ids = _.reduce(first, function (a, b) {
                return _.union(a, b);
            }, []);

            return model.find({_id: {$in: ids}});
        }

        function getOpt(opt, def) {
            return options[opt] || def;
        }
    },

    hasA: function (model, name) {
        if (name === undefined) {
            name = model._name;
        }

        var field = "_" + name;
        this.defaults[field] = null;

        this.prototype["get" + Util.ucfirst(name)] = function () {
            return this[field] == null ? null :
                model.collection.findOne({_id: this[field]});
        }

        this.prototype["is" + Util.ucfirst(name)] = function (r) {
            var r_id = typeof r == "object" ? r._id : r;
            return this["get" + Util.ucfirst(name)]() == r_id;
        }

        this.prototype["set" + Util.ucfirst(name)] = function (r) {
            var r_id = typeof r == "object" ? r._id : r;
            this[field] = r_id;
            this.save();
        }
    },

    find: function (query) {
        return this.collection.find(query).fetch();
    }
}
